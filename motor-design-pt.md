# Motor

Motor (nome provisório) é uma biblioteca de [Sistema Entidade Componente][ecs-wikipedia] com gerenciamento de Estados para 
a [linguagem de programação Lua][lua-org] (Lua 5.3 e [LuaJIT][luajit-org]).

É muito inspirada pelo [specs][specs], [Amethyst][amethyst] e pela [palestra da Catherine West na RustConf][kyren-rustconf-video]
([blogpost][kyren-rustconf-blogpost]), mas não pretende ser uma implementação igual dos mesmos.

Motor é composto de 7 conceitos: Entidades, Componentes, Recursos, Armazenamentos, Sistemas, Estados e um Gerenciador de Estados, conforme detalhados abaixo:

* Uma entidade é composto de 3 elementos:
    * Simples número identificador único (ou seja, uma ID), que é um índice geracional. Este é o único dos 3 que o usuário tem acesso e representa a entidade em si.
    * Número binário que guarda quais combinações de componentes estão sendo usadas.
    * Arranjo com os índices dos componentes associados.
* Um Componente é composto de 3 elementos:
    * Componente em si, que é um simples registro 
    * Manipulador do componente, com funções de manipulação
    * Construtor, uma função que recebe valores iniciais e constrói um novo componente (ou seja, um .new(params))
* Um armazenamento é uma lista de componentes, é quem guarda todos os componentes em execução de um componente especifico (por exemplo, um armazenamento de componentes de transformação). As entidades também são guardadas num armazenamento. Um Armazenamento é guardado num recurso.
* Um recurso é um campo que guarda algum registro ou valor e é guardada dentro de um estado.
* Um sistema é onde ocorre todo o processamento, ele conterá dependências de quais componentes ele lerá e escreverá como resultado de seu processamento, também pode esperar por outros sistemas antes de sua execução, por exemplo, um sistema de movimentação de jogador deve esperar que o sistema de entrada (input) seja finalizada, mas isso é definido no estado, que é onde o sistema é guardado.
* Um estado é quem guarda todos os recursos e sistemas cadastrados, é executando um estado que todos os sistemas dentro dele serão executados.
* O gerenciador de estados é uma máquina de estados única numa instância do Motor, ele guarda um ou mais estados, e permite adicionar ou remover estados do tempo de execução.

# Desenho do Motor

A seguir está documentada o desenho do Motor, o intuito desse tópico é explicar como o Motor funciona internamente.

## Armazenamento

Um armazenamento é em sua essência uma lista de registros, ou seja, se o registro é descrita assim (usando pseudo-código):

<div id="codeblock">

```Lua
MyStruct: {
    x: number,
    y: number
}
```
    
</div>

No armazenamento, será guardada da seguinte forma:

<div id="codeblock">

```Lua
MyStructInStorage: {
    local free_indexes: [numbers],
    local entries: [MyStruct] = [
        { -- 0
            x: number,
            y: number
        },
        { -- 1  
            x: number,
            y: number
        },
        ...
    ]
}
```

</div>


Os registros são guardados e identificados usando [índices geracionais](https://kyren.github.io/2018/09/14/rustconf-talk.html#generational-indexes-are-awesome),
por isso, para obter ou modificar um registro é necessário usar um índice geracional.

## Entidades

Entidades vivem dentro do armazenamento de entidades.  
A definição de um registro de uma entidade é:

<div id="codeblock">

```Lua
Entity: {
    -- array with *n* elements, this *n* is defined in the state creation.
    local component_filter: [number; n],
    local associated_components: [number]
}
```

</div>

O membro `component_filter` identifica quais componentes estão associados à entidade e o `associated_components` as localizações desses componentes associados.

|  entities | transforms | velocities | sprites | component filters | associated components |
|:---------:|:----------:|:----------:|:-------:|:-----------------:|:---------------------:|
| entity #1 |    T[1]    |    V[1]    |   S[1]  |   [0b000000111]   |       [1, 1, 1]       |
| entity #2 |    T[2]    |            |         |   [0b000000100]   |       [2]             |
| entity #3 |    T[3]    |    V[2]    |   S[2]  |   [0b000000111]   |       [3, 2, 2]       |
| entity #4 |    T[4]    |    V[3]    |         |   [0b000000110]   |       [4, 3]          |

Por padrão, números em Lua contém 64 bits, porém ela pode ser compilada de forma que use 32 bits, sendo assim, se fosse apenas um número,
seu estado apenas poderia comportar de 32 ou 64 componentes, por isso a entidade pode conter um arranjo de n filtros conforme a necessidade
através de um parâmetro opcional na criação do estado, caso o parâmetro seja `nil` ou `1`, o acesso desse filtro será simplificado por otimização.

No exemplo acima a entidade #3 teria em seu `associated_components` os valores [3, 2, 2], que são os índices dos componentes
(obs: lembrando que o que serão usados aqui serão os índices geracionais e não índices simples como demonstrado no exemplo).

Todos esses aspectos são controlados automaticamente pelo Motor.

## Componentes

Um componente é um registro feito pelo usuário, como por exemplo:

<div id="codeblock">

```Lua
MovementComponent: {
    velocity: number,
    direction: vector2
}
```

</div>

## Sistemas

Um sistema vive diretamente sob um estado e é quem processa os componentes:

<div id="codeblock">

```Lua
local Transform = require "components.transform"
local Movement  = require "components.movement"

MovementSystem = Motor.new_system({
    read = {Movement.name},
    write = {Transform.name}
})

function MovementSystem.run (entities)
    for movement, transform in entities:iterate_components() do
        Transform.translate(transform, movement.direction * movement.velocity)
    end
end
```

</div>
    
## Estados

Um estado é justamente um contêiner que guarda os sistemas e os recursos (com os componentes e entidades dentro). 
Depedências entre sistemas podem ser definidos através do estado.

Para executar os sistemas de um estado, basta executar `state:run()`.

## Gerenciamento de Estados

É a estrutura de maior nível, ele guarda e executa um ou mais estados ao mesmo tempo ou não, porém,
ainda não estou certo se a execução de mais de um estado é algo desejável, e ainda não sei como será o seu gerenciamento,
o [Amethyst usa autômato com pilha](https:--www.amethyst.rs/book/master/concepts/state.html#state-manager),
eu ainda não me decidi se esta é realmente a melhor forma e pesquisarei mais sobre isso em breve.
Por conta disso, o Motor apenas te dará o poder de fazer sua implementação, através das funções `pause(state)` e `resume(state)`.

[ecs-wikipedia]: https:--pt.wikipedia.org/wiki/Entity-component-system "Artigo do Wikipédia sobre ECS"
[lua-org]: http:--lua.org/ "Site oficial da linguagem de programação Lua"
[luajit-org]: http:--luajit.org/ "Site oficial do LuaJIT, um compilador JIT para Lua"
[specs]: https:--github.com/slide-rs/specs/ "Repositório do specs no Github"
[amethyst]: https:--github.com/amethyst/amethyst "Repositório do Amethyst no Github" 
[kyren-rustconf-video]: https:--www.youtube.com/watch?v=aKLntZcp27M "Palestra da RustConf da Catherine West no Youtube"
[kyren-rustconf-blogpost]: https:--kyren.github.io/2018/09/14/rustconf-talk.html "Blogpost da Catherine West da sua palestra na RustConf"